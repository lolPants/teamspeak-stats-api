// Package Dependencies
const Query = require('teamspeak-query')
const backoff = require('backoff')

// Environment Variables
const { TS_HOST, TS_PORT, TS_V_ID } = process.env

// Setup Query Interface
if (!TS_HOST) throw new Error('TeamSpeak host IP not specifed.')
if (!TS_V_ID) throw new Error('TeamSpeak virtualserver ID not specifed.')

/**
 * @param {Function} func Async Function to Perform Backoff
 * @returns {Promise}
 */
const awaitBackoff = func => new Promise((resolve, reject) => {
  const fib = backoff.fibonacci({
    randomisationFactor: 0.5,
    initialDelay: 200,
    maxDelay: 10000,
  })
  fib.failAfter(10)

  fib.on('ready', async () => {
    try {
      const data = await func()
      resolve(data)
    } catch (err) {
      fib.backoff()
    }
  })

  fib.on('fail', () => {
    reject(new Error('Max Retries Reached.'))
  })

  fib.backoff()
})

/**
 * Fetch info about the TeamSpeak Virtualserver
 * @returns {Promise}
 */
const fetchServerInfo = async () => {
  // Login and use virtualserver
  const query = new Query(TS_HOST, TS_PORT || 10011)
  await query.send(`use ${TS_V_ID}`)
  await query.send('clientupdate', { client_nickname: 'TeamSpeak Stats API' })

  // Get Raw Info
  let info = await query.send('serverinfo')
  let clients = await query.send('clientlist')

  // Update clientsonline to remove ServerQuery Clients
  if (Array.isArray(clients.client_type)) {
    info.virtualserver_clientsonline = clients.client_type.filter(x => x === '0').length
  } else {
    info.virtualserver_clientsonline = clients.client_type === '0' ? 1 : 0
  }

  // Parse Numbers
  for (let i in info) {
    let x = info[i]
    info[i] = isNaN(x) ? x : +x
  }

  // Cleanup
  await query.send('quit')
  return info
}

const fetchChannelUsers = async () => {
  // Login and use virtualserver
  const query = new Query(TS_HOST, TS_PORT || 10011)
  await query.send(`use ${TS_V_ID}`)
  await query.send('clientupdate', { client_nickname: 'TeamSpeak Stats API' })

  const info = await query.send('channellist')
  const channels = Array.from(new Array(info.cid.length))
    .map((x, i) => ({
      name: info.channel_name[i],
      clients: info.total_clients[i],
      id: info.cid[i],
      parent_id: info.pid[i] === '0' ? null : info.pid[i],
    }))

  // Cleanup
  await query.send('quit')
  return channels
}

// Wrapped Functions
const queryServerInfo = () => awaitBackoff(fetchServerInfo)
const queryChannelUsers = () => awaitBackoff(fetchChannelUsers)

module.exports = {
  queryServerInfo,
  queryChannelUsers,
}
