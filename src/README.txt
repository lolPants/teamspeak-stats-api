TeamSpeak Stats API
Usage:

GET /api/v1.0/info
Cached: 5 Minutes
Returns a JSON object of virtualserver info

GET /api/v1.0/online
Cached: 2 Minutes
Returns a raw number of online clients (excluding query clients)

GET /api/v1.0/channels
Cached: 2 Minutes
Returns a JSON array of channels. Keys are 'name' and 'clients'
name:    Channel name
clients: Current clients online (incl. query clients)
