// Package Dependencies
const path = require('path')
const express = require('express')
const routeCache = require('route-cache')
const cors = require('cors')

// Local Dependencies
const { queryServerInfo, queryChannelUsers } = require('./query.js')

// Express app
const app = express()
app.use(cors())

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'README.txt'))
})

app.get('/api/v1.0/info', routeCache.cacheSeconds(5 * 60), async (req, res) => {
  try {
    res.send(await queryServerInfo())
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.get('/api/v1.0/online', routeCache.cacheSeconds(2 * 60), async (req, res) => {
  try {
    res.send(`${(await queryServerInfo()).virtualserver_clientsonline}`)
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.get('/api/v1.0/channels', routeCache.cacheSeconds(2 * 60), async (req, res) => {
  try {
    res.send(await queryChannelUsers())
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

// Serve HTTP
app.listen(process.env.PORT || 3000)
