# TeamSpeak Stats API ![](https://gitlab.com/lolPants/teamspeak-stats-api/badges/master/build.svg)
_Basic TeamSpeak Stats Reporting API_  
Built by [Jack Baron](https://www.jackbaron.com)

## Installation
### Prerequisites
* Docker

### Setup
1. Make sure Docker Daemon is running with `docker ps`
2. Pull the docker image:  
`docker pull registry.gitlab.com/lolpants/teamspeak-stats-api:latest`
3. Create `tsapi.env` and fill in your details *(see below)*.
4. Start the service using:  
`docker run -p 80:3000 --restart on-failure --name tsapi -d --env-file tsapi.env registry.gitlab.com/lolpants/teamspeak-stats-api:master`

## `tsapi.env`
To configure your bot, make a new file named `tsapi.env` and fill it out as follows:
```env
# TeamSpeak Host IP
TS_HOST=ts.n3s.co

# TeamSpeak Host Port
# If not specifed defaults to 10011
TS_PORT=10011

# TeamSpeak virtualserver ID
TS_V_ID=2
```
